=begin comment

This module contains the subroutines that define the custom character
classes used in many of Diogenes' regular expressions. All symbols are
exported by default.

=cut

package Diogenes::CharSet;

use Exporter;

@ISA = qw(Exporter);
@EXPORT = qw($IsBetaCon $IsBetaVow $IsBetaDia $IsBetaLet $IsBetaChr $IsLatLet);


$IsBetaCon = 
'\\x42\\x43\\x44\\x46\\x47\\x4b\\x4c\\x4d\\x4e\\x50\\x51\\x52\\x53\\x54\\x56\\x58\\x59\\x5a';

$IsBetaVow =
'\\x41\\x45\\x48\\x49\\x4f\\x57\\x55';

$IsBetaDia = 
'\\x2f\\x5c\\x3d\\x2b\\x3f\\x29\\x28\\x7c';

$IsBetaLet = 
"${IsBetaCon}${IsBetaVow}";

$IsBetaChr =
"${IsBetaLet}${IsBetaDia}";

$IsLatLet = 
'\\x41\\x42\\x43\\x44\\x45\\x46\\x47\\x48\\x49\\x4a\\x4b\\x4c\\x4d\\x4e\\x4f\\x50\\x51\\x52\\x53\\x54\\x55\\x56\\x58\\x59\\x5a\\x61\\x62\\x63\\x64\\x65\\x66\\x67\\x68\\x69\\x6a\\x6b\\x6c\\x6d\\x6e\\x6f\\x70\\x71\\x72\\x73\\x74\\x75\\x76\\x78\\x79\\x7a';
