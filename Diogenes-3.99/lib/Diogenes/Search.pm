package Diogenes::Search;

use strict;
use warnings;
use Diogenes::Base qw(%work %author %work_start_block %level_label %context);
use Diogenes::CharSet;

@Diogenes::Search::ISA = ('Diogenes::Base');

sub do_search
{
    my $self = shift;

    # Do the search (brute force).
    $self->begin_boilerplate;
    $self->pgrep;
    $self->print_totals;
    $self->end_boilerplate;
}

###############################################################
#                                                             #
# Method that actually does the brute-force searches.         #
# For an explanation of the approach using @ARGV, see p. 226  #
# of /Programming Perl/, 2nd edition.                         #
#                                                             #
# It takes a pattern and returns a hash of filenames, each    #
# pointing to an array containing the offsets of the matches. #
#                                                             #
###############################################################

sub pgrep
{
    my $self = shift;

    chdir "$self->{cdrom_dir}" or
        $self->barf ("cannot chdir to $self->{cdrom_dir} ($!)");
    print STDERR "\nCurrent dir: ",`pwd`,
    "Prefix: $self->{file_prefix}\n\n" if $self->{debug};

    unless ($self->{filtered})
    {
        # In theory, the following code is platform independent for
        # achitectures (ie. Mac) that don't like unix-shell style globbing.
        opendir (INP_DIR, "$self->{cdrom_dir}") or
            $self->barf ("Cannot open $self->{cdrom_dir} ($!)");
        @ARGV = grep {/$self->{file_prefix}.+$self->{txt_suffix}/i} readdir INP_DIR;
        closedir INP_DIR;
    }

    $self->barf ("I can't find any data files!")
        unless @ARGV or $self->{req_auth_wk};
    local $/;
    undef $/;

    @{ $self->{pattern_list} } = sort { scalar length($b->{pat}) <=> scalar length($a->{pat}) } @{ $self->{pattern_list} }
      if @{ $self->{pattern_list} } > 1;
    my ($buf, $i);
    $self->{buf} = \$buf;
    $self->read_blacklist if $self->{blacklist_file};
    $self->read_works_blacklist if $self->{blacklisted_works_file};

    if (@ARGV)
    {
      AUTH:
        while ($buf = <>)
        {
            # Before each pass, make sure that the browser is still listening;
            # SIGPIPE does not work under MS Windows
            return if $Diogenes_Daemon::flag and not print ("\0");

            if (@{ $self->{pattern_list} } > 1) {
                for my $pathash (@{ $self->{pattern_list} }) {
                    next AUTH unless $self->matches(\$buf, $pathash);
                    undef pos($buf);
                }
            }

            my ($pat, $sub) = ($self->{pattern_list}[0]{pat}, $self->{pattern_list}[0]{lookback});

            while ($buf =~ /$pat/gp) {
                push @{ $self->{seen} }, [(pos $buf) - length ${^MATCH}, pos $buf]
                  if $sub->(substr ${^PREMATCH}, -200);
            }
            if ($self->{seen})
            {
                my $auth_num = $ARGV;
                $auth_num =~ tr/0-9//cds;
                $self->parse_idt($auth_num);
                $self->extract_hits($ARGV);
            }
            undef $self->{seen};
        }
    }

  # Now search in any files that are to be read only in part.

    # Read in only the desired blocks from files for which only certain works
    # were requested.

    my ($filename, $offset, $start_block, $end_block);
    foreach my $author (keys %{ $self->{req_auth_wk} })
    {
        # pad with leading zeroes
        $filename = sprintf '%04d', $author;

        # parse .idt file
        my $real_num = $self->parse_idt($filename);

        $filename = $self->{file_prefix} . $filename;

        # open the .txt file
        $filename .= $self->{txt_suffix};
        open INP, "$self->{cdrom_dir}$filename" or $self->barf("Couln't open $filename!");
        binmode INP;

        # loop through each requested work

      WORK:
        foreach my $work (sort keys %{ $self->{req_auth_wk}{$author} })
        {
            # get only those blocks of the file containing the work in question
            $start_block = $work_start_block{$self->{type}}{$real_num}{$work};
            $offset = $start_block << 13;
            seek INP, $offset, 0;
            my $next = $work;
            $next++;
            if (defined ($work_start_block{$self->{type}}{$author}{$next}))
            {
                $end_block = $work_start_block{$self->{type}}{$author}{$next};
                read INP, $buf, (($end_block - $start_block + 1) << 13) or
                    $self->barf ("Couln't read from $filename");
            }
            else
            {
                $buf = <INP>;
                $self->barf ("Couln't read the rest of $filename!") unless
                    defined $buf;
            }
            $self->{current_work} = $work;

            return if $Diogenes_Daemon::flag and not print ("\0");

            if (@{ $self->{pattern_list} } > 1) {
                for my $pathash (@{ $self->{pattern_list} }) {
                    next WORK unless $self->matches(\$buf, $pathash);
                    undef pos($buf);
                }
            }

            my ($pat, $sub) = ($self->{pattern_list}[0]{pat}, $self->{pattern_list}[0]{lookback});
            # clear the last search
            undef $self->{seen};

            while ($buf =~ m#$pat#gp) {
                push @{ $self->{seen} }, [(pos $buf) - length ${^MATCH}, pos $buf]
                  if $sub->(substr ${^PREMATCH}, -200);
            }

            $self->extract_hits($author) if @{ $self->{seen} };
        }
        close INP or $self->barf("Couln't close $filename!");
    }
    return 1;
}


###########################################################
#                                                         #
# Method to print the total hits in a brute force search. #
#                                                         #
###########################################################

sub print_totals
{
    my $self = shift;
    my $out = '';
    $out .= '\nrm{}' if $self->{output_format} eq 'latex';
    $out .= (@{$self->{pattern_list}} > 1 ? "\n\&Passages found: " : "\n\&Matches found: ")
      . ($self->{hits} || 0) ."\n";
    $out .= '(' . $self->{blacklisted_hits} .
        " passages suppressed from blacklisted works)\n"
        if $self->{blacklisted_hits};
    $out .= '(' . $self->{blacklisted_files} .
        " blacklisted authors were not searched at all)\n"
        if $self->{blacklisted_files};
    $out .= "\n";
    $self->print_output(\$out);
}

# Non-unicode, various Latin transliterations

sub make_greek_patterns_translit {
    my $self = shift;
    $self->{reject_pattern} &&=
        $self->make_greek_pattern_from_translit($self->{reject_pattern});
    @{ $self->{pattern_list} } = map {
        if ($_ eq '--strict' && ! $self->{strict_grk_matching}) {
            $self->{strict_grk_matching} = 1;
            ();
        }
        else { $self->make_greek_pattern_from_translit($_) }
    } @{ $self->{pattern_list} };
}

sub make_greek_pattern_from_translit {
    my ($self, $pat) = @_;
    if ($self->{input_pure}) {
        return { pat => $pat, lookback => sub { 1 } };
    }
    elsif ($self->{input_raw}) {
        return { pat => quotemeta($pat), lookback => sub { 1 } };
    }
    elsif ($self->{input_beta}) {
        return $self->{strict_grk_matching} || $pat =~ m/[\\\/=()|?+]/
               ? $self->mk_strict_grk_pat($pat)  # accent(s) present, so significant
               : $self->mk_grk_pat($pat);
    }
    else {
        # Fall back to Perseus transliteration
        return $self->perseus_to_beta($pat);
    }
}

{

# Skip instances of recursive { ... { ... } ... }
my $ignore_brkt_rx = qr< (
                           \{
                           [^{}]*+
                           (?:
                               \}\d*+
                              |
                               (?-1)
                           )
                         )
                       >x;

=begin comment

Construct regex from beta-code, matching accents, breathings, etc. exactly as
specified (except that acute and grave accents are normalized). As above,
return anonymous array of the pattern and subroutine to emulate
beginning-of-word assertion.

=cut

sub mk_strict_grk_pat {
    use if $] >= 5.017, re => '/p';

    my ($self, $pat) = @_;

    # rx to go between tokens, i.e. letter-breathing clusters belonging to the same word
    my $inter_tok_rx = qr< (?(?=-)-  # when the next byte is a dash, grab it, and...
                                    (?:
                                        [^${IsBetaChr}'*{}]++ $ignore_brkt_rx?+
                                    )++  # grab at least one non-beta char or { ... } construct

                                   |  # when the next byte is not a dash...
                                    (?:
                                        [^${IsBetaChr} _'*{}]
                                       |
                                        $ignore_brkt_rx
                                    )*+  # grab all consecutive non-beta chars, spaces, and { ... } constructs
                           )
                         >x;

    # rx to go between words
    my $inter_word_rx = qr<
                            [^- _${IsBetaChr}'*]*+ [ _]
                            (?:
                                [^${IsBetaLet}*{}]
                               |
                                $ignore_brkt_rx
                            )*+
                          >x;

    my ($let, $bre, $acc, $dot, $dia, $iot, $apo);
    my $init_space = $pat =~ s/^\s+// ? 1 : 0;
    my $end_space = $pat =~ s/\s+$// ? 1 : 0;

    $pat =~ tr/a-z/A-Z/;
    $pat =~ s#\\#/#g;
    $pat =~ s#\s+# #g;

    my @words = split ' ', $pat;
    for my $word (@words) {
        # Split the word into letter-diacritic-apostrophe tokens.
        my @tokens = split / (?= [${IsBetaLet}] ) /x, $word;
        for (@tokens) {
            # Break the token up into letter, breathing, accent,
            # underdot, diaeresis, iota subscript, apostrophe.
            ($let, $bre, $acc, $dot, $dia, $iot, $apo) = $_ =~ m# (.) ( [()]? ) ( [/\\=]? ) ( \?? ) ( \+? ) ( \|? ) ( '? ) #x;

            # Accents, breathings, etc. occur at different places
            # relative to the letter they modify, depending on the
            # case of the letter. Ensure case-insensitive matching.
            if ( / ^.'?$ /x ) { $_ = qr/\*?+$_/ }
            else {
                $acc =~ s#/#[\\\\/]#g;
                $acc =~ s#=#\\=#g;
                $_ = qr<
                         (?: $let \Q$bre\E $acc \Q$dia$dot\E
                            |
                             \* \Q$bre\E $acc \Q$dia$dot\E $let
                         )
                         \Q$iot\E $apo
                       >x;
            }
        }
        $word = join $inter_tok_rx, @tokens;
    }
    $pat = join $inter_word_rx, @words;
    my $lookback_sub = $init_space ?
                       \&strict_lookback :
                       substr ($pat, 0, 5) =~ /\\\*\?\+[${IsBetaVow}]/ ?
                       \&protect_vow :
                       sub { 1 };
    $pat = $end_space ? qr< $pat (?! [-${IsBetaChr}'*{}] ) >x : qr< $pat (?![${IsBetaDia}]) >x;
    return { pat => $pat, lookback => $lookback_sub };
}


=begin comment

Construct regex from beta-code to search for Greek. Accents and breathings,
including iota-subscript, are not significant. A different subroutine is used
for word-list searches. Return anonymous array of the pattern and the subroutine
to emulate beginning-of-word assertion if the parameter passed to the program
began with whitespace.

=cut

sub mk_grk_pat {
    use if $] >= 5.017, re => '/p';

    my ($self, $pat) = @_;

    my $inter_tok_rx = qr<
                           [${IsBetaDia}]*+
                           (?(?=-)-
                                    (?:
                                        [^${IsBetaLet}'{}]++ $ignore_brkt_rx?+
                                    )++
                                   |
                                    (?:
                                        [^${IsBetaLet} _'{}]*+
                                       |
                                        $ignore_brkt_rx
                                    )*+
                           )
                         >x;

    my $inter_word_rx = qr<
                            [^- _${IsBetaLet}']*+ [ _]
                            (?:
                                [^${IsBetaLet}{}]
                               |
                                $ignore_brkt_rx
                            )*+
                          >x;

    my $init_space = $pat =~ s/^\s+// ? 1 : 0;
    my $end_space = $pat =~ s/\s+$// ? 1 : 0;

    $pat =~ tr/a-z/A-Z/;
    $pat =~ s#\s+# #g;

    my @words = split ' ', $pat;
    foreach my $word (@words) {
        # Split the word into letter-apostrophe tokens.
        my @tokens = split / (?= [${IsBetaLet}] ) /x, $word;
        map { $_ = /([${IsBetaVow}])'/ ? $1 . "[${IsBetaDia}]*+'" : $_ } @tokens;
        $word = join $inter_tok_rx, @tokens;
    }
    $pat = join $inter_word_rx, @words;
    $pat = qr< (?: \* [${IsBetaDia}]*+ )?+ $pat [${IsBetaDia}]*+ >x;
    $pat = qr< $pat (?! [-${IsBetaLet}'*{}] ) >x if $end_space;
    return { pat => $pat, lookback => $init_space ? \&strict_lookback : sub { 1 } };
}


sub latin_pattern {
    use if $] >= 5.017, re => '/p';

    my ($self, $pat) = @_;
    my $inter_tok_rx = qr<
                           (?:
                               [^ _\-\$${IsLatLet}{}]
                              |
                               $ignore_brkt_rx
                           )*+
                           (?(?=-)
                                   (?:
                                       [^\$${IsLatLet}{}]++ $ignore_brkt_rx?+
                                   )++
                                  |
                                   (?:
                                       [^ _\-\$${IsLatLet}{}]
                                      |
                                       $ignore_brkt_rx
                                   )*+
                           )
                         >x;

    my $inter_word_rx = qr<
                            [^- _\$${IsLatLet}]*+ [ _]
                            (?:
                                [^-${IsLatLet}\${}]
                               |
                                $ignore_brkt_rx
                            )*+
                          >x;
    my $init_space = $pat =~ s/^\s+// ? 1 : 0;
    my $end_space = $pat =~ s/\s+$// ? 1 : 0;

    $pat =~ tr/A-Z/a-z/;

    my @words = split /[ \t]+/, $pat;
    for my $word (@words) {
        my @tokens = split / (?= [${IsLatLet}] ) /x, $word;
        for (@tokens) {
            if    (/[uv]/) { $_ = '[uUvV]' }
            elsif (/[ij]/) { $_ = '[iIjJ]' }
            else           { $_ = "[$_" . uc($_) . "]" }
        }
        $word = join $inter_tok_rx, @tokens;
    }
    $pat = join $inter_word_rx, @words;
    $pat = $end_space ? qr< $pat (?!
                                     (?:
                                         [^ _\-\$${IsLatLet}{}]
                                        |
                                         $inter_tok_rx
                                     )*+
                                     [-${IsLatLet}]
                                 )
                          >x
                            : qr/$pat/;

    return { pat => $pat, lookback => $init_space ? \&lat_lookback : sub { 1 } };
}

}

sub make_latin_pattern
{
    my $self = shift;

    $self->{reject_pattern} = $self->latin_pattern($self->{reject_pattern});
    foreach my $pat (@{ $self->{pattern_list} })
    {
        if ($self->{input_pure}) {
            $pat = { pat => $pat, lookback => sub { 1 } };
        }
        elsif ($self->{input_raw}) {
            $pat = { pat => quotemeta($pat), lookback => sub { 1 } };
        }
        else {
            $pat = $self->latin_pattern($pat);
        }
    }
}

#####################################################################
#                                                                   #
# Subroutine to convert latinized greek to a useful search pattern. #
# Input is in Perseus format, output is TLG BETA code regexp.       #
#                                                                   #
#####################################################################

sub perseus_to_beta
{
    my $self = shift;
    $_ = shift;

    # FIXME: When would this occur? Shouldn't it be `return { pat => $_,
    # lookback => sub { 1 } }?
    return $_ unless $_;

    s#\(#\x073#g;                                                       # protect parens
    s#\)#\x074#g;

    die "Invalid letter $1 used in Perseus-style transliteration\n" if m/([wyjqv])/;
    die "Invalid letter c used in Perseus-style transliteration\n" if m/c(?!h)/;
    # This is now entirely case-insensitive (and ignorant of accent).
    # The business of having accents and breathings before caps
    # made it nearly impossible to do case- sensitive searches reliably, and
    # the best candidate regeps were an order of magnitude slower.

    s/\b([aeêioôu\xea\xf4])/\x071$1/gi;         # mark where non-rough breathing goes
    s/(?<!\w)h/\x072/gi;                        # mark where rough breathing goes

#       #s#\b([aeêioôu^]+)(?!\+)#(?:\\\)[\\/|+=]*$1|$1\\\))#gi;         # initial vowel(s), smooth
#
#       s#^h# h#; # If there's a rough breathing at the start of the pattern, then we assume it's the start of a word
#   s#(\s)([aeêioôu^]+)(?!\+)#$1(?:(?<!\\\([\\/|+=][\\/|+=])(?<!\\\([\\/|+=])(?<!\\\()$2(?!\\\())#gi;           # initial vowel(s), smooth
#       s#(\s)h([aeiou^]+)(?!\+)#$1(?:\\\([\\/|+=]*$2|$2\\\()#gi;             # initial vowel(s), rough breathing
#   s#\bh##; # Ignore breathings

    s/[eE]\^/H/g;                                           # eta
    s/[êÊ\xea\xca]/H/g;                                             # ditto
    s/[tT]h/Q/g;                                            # theta
    s/x/C/g;                                                # xi
    s/[pP]h/F/g;                                            # phi
    s/[cC]h/X/g;                                            # chi
    s/[pP]s/Y/g;                                            # psi
    s/[oO]\^/W/g;                                           # omega
    s/[ôÔ\xf4\xd4]/W/g;                                             # ditto

    return $self->mk_grk_pat($_);
}

# h looks ahead for a rough breathing somewhere in the following group
# of vowels, or before it as capitalized words or in the word (EA`N.
# In lookbehind, allow for other diacrits to intervene

my $rough_breathing =  q{(?:(?<=\()|(?<=\([^A-Z])|(?<=\([^A-Z][^A-Z])|(?=[AEHIOWU/\\\\)=+?!|']+\())};
my $smooth_breathing =  q{(?:(?<=\))|(?<=\)[^A-Z])|(?<=\)[^A-Z][^A-Z])|(?=[AEHIOWU/\\\\)=+?!|']+\)))};

# Latin transliteration input, except without turning the greek into a
# regexp (Used on input destined for the word list.)

sub simple_latin_to_beta
{
    my $self = shift;
    $_ = shift;
    return quotemeta $_ if $self->{input_raw} or $self->{input_pure};
    if ($self->{input_beta})
    {
        tr/a-z/A-Z/;                                            # upcap all letters
        s#\\#/#g;
        $_ = quotemeta $_;
        s#\\\s+# #g;
        return $_;
    }

    # Fall back to Perseus-style transliteration

    tr/A-Z/a-z/;
    my $start;
    $start++ if s#^\s+##;

    s/\b([aeêioôu\xea\xf4])/\x071$1/g;     # mark where non-rough breathing goes
    s#^h#\x072#i;                  # protect h for rough breathing later

    s/[eE]\^/H/g;                                           # eta
    s/[êÊ\xea\xca]/H/g;
    s/[tT]h/Q/g;                                            # theta
    s/x/C/g;                                                # xi
    s/[pP]h/F/g;                                            # phi
    s/[cC]h/X/g;                                            # chi
    s/[pP]s/Y/g;                                            # psi
    s/[oO]\^/W/g;                                           # omega
    s/[ôÔ\xf4\xd4]/W/g;
#   if (/h/) { $self->barf("I found an \`h\' I didn't understand in $_")};
    tr/a-z/A-Z/;                                            # upcap all other letters

    s#\x072#$rough_breathing#g;
    s#\x071#$smooth_breathing#g;

    s#^# # if $start; # put the space back in front
    return $_;
}


##################################################################
#                                                                #
# Method to remove from @ARGV any files that match the blacklist #
# of files that we never want to search through.                 #
#                                                                #
##################################################################

sub read_blacklist
{
    my $self = shift;
    my $bl;
    open BL, "<$self->{blacklist_file}" or
        die "Couldn't open blacklist file: $self->{blacklist_file}: $!\n";
    {
        local $/;
        undef $/;
        $bl = <BL>;
    }
    print STDERR "Files originally in ARGV: ".scalar @ARGV."\n" if $self->{debug};
    my @files;
    $self->{blacklisted_files} = 0;
    foreach my $file (@ARGV)
    {
        $file =~ m/($self->{file_prefix}\d\d\d\d)/;
        my $pat  = $1;
        if ($bl =~ m/$pat/i)
        {
            print STDERR "Removing blacklisted file: $file\n" if $self->{debug};
            $self->{blacklisted_files}++;
        }
        else
        {
            push @files, $file;
        }
    }
    @ARGV = ();
    @ARGV = @files;
    print STDERR "Files remaining in ARGV: ".scalar @ARGV."\n" if $self->{debug};
}

sub read_works_blacklist
{
    my $self = shift;
    open BL, "<$self->{blacklisted_works_file}" or
        die "Couldn't open blacklisted works file: $self->{blacklisted_works_file}: $!\n";
    {
        local $/;
        $/="\n";

    while (my $entry = <BL>)
    {
            chomp $entry;
            next if $entry =~ m/^\s*$/;
            next if $entry =~ m/^#/;
            my ($auth, @works) = split ' ', $entry;
            die "Bad blacklisted works auth ($self->{blacklisted_works_file}): $auth\n"
                unless $auth =~ m/^\D\D\D\d+$/;
            my ($type, $auth_num) = $auth =~ m/^(\D\D\D)(\d+)$/;
            $type = lc $type;
            $auth_num = sprintf '%04d', $auth_num;

            for (@works)
            {
                die "Bad blacklisted work ($self->{blacklisted_works_file}): $_\n"
                    unless $_ =~ m/^\d+$/;
                $_ = sprintf '%03d', $_;
                warn "Blacklisting $type$auth_num: $_\n" if $self->{debug};
                $self->{blacklisted_works}{$type}{$auth_num}{$_} = 1;
            }
        }
    }
    $self->{blacklisted_hits} = 0;
    close BL or
        die "Couldn't close blacklisted works file: $self->{blacklist_file}: $!\n";
}


##################################################################################
#                                                                                #
# Subroutine to seek to the position of each hit in a search and for each one to #
# extract the location info and surrounding context.                             #
#                                                                                #
##################################################################################

sub extract_hits {
    my ($self, $auth) = @_;
    my $hits = $self->{seen};
    my ($beg, $end, $result_ref, $output_ref);

    for (my ($match, $nhits) = (0, scalar @{$hits}); $match < $nhits; $match++) {
        my $first_match = $hits->[$match][0];
        ($beg, $end, $result_ref) = $self->get_context($match, $auth);
        if ($self->{reject_pattern} && $self->do_reject($result_ref)) {
            1 while $match < $nhits - 1 and $hits->[$match + 1][1] <= $end and ++$match;
            next;
        }
        next unless $self->highlight_hits($result_ref, $beg, $end, \$match, $hits);

        my $buf = $self->{buf};
        my $block_start = ($first_match >> 13) << 13;
        for (my ($i, $code) = ($block_start, 0); $i < $first_match; $i++)  {
            # parse all of the data in this block from its beginning to our match
            $code = ord( substr($$buf, $i, 1) );
            next unless $code >> 7;
            $self->parse_non_ascii($buf, \$i);
        }

        next unless ($output_ref = $self->prepare_output($beg, $result_ref));
        $self->print_output($output_ref);
    }
}

sub highlight_hits
{
    my ($self, $buf, $beg, $end, $match_ref, $hits) = @_;
    my ($nhits, $pats) = (scalar @{$hits}, $self->{pattern_list});

    for my $pathash ( @{$pats}[1 .. $#{$pats}] ) {
        unless ($self->matches($buf, $pathash)) {
            1 while $$match_ref < $nhits - 1 and $hits->[$$match_ref + 1][1] <= $end and ++$$match_ref;
            return 0;
        }
        undef pos($$buf);
    }

    my $n = 0;
    do {
        substr($$buf, $hits->[$$match_ref][1] - $beg + $n * 6, 0, '~');
        substr($$buf, $hits->[$$match_ref][0] - $beg + $n * 6, 0, '~hit~');
        $n++;
    } while $$match_ref < $nhits - 1 and $hits->[$$match_ref + 1][1] <= $end and ++$$match_ref;


    my ($pat, $sub);
    for my $pathash ( @{$pats}[1 .. $#{$pats}] ) {
        ($pat, $sub) = (\$pathash->{pat}, $pathash->{sub});
        $$buf =~ s#(~hit~[^~]*~)|($$pat)#
           if ($1) { $1 }
         else      {
                     if (not $sub or $sub->(${^PREMATCH})) { "~hit~$2~" }
                     else { $2 }
                   }
                          #gexp;
    }

    $self->{hits} += @{$pats} > 1 ? 1 : $n;
    return 1;
}

sub matches
{
    my ($self, $buf, $pathash) = @_;
    my ($pat, $sub) = (\$pathash->{pat}, $pathash->{lookback});

    while ($$buf =~ m#$$pat#gp) { return 1 if $sub->(substr ${^PREMATCH}, -200) }

    return 0;
}

sub get_context
{
    my ($self, $match, $auth) = @_;
    my ($beg, $end, $lines, $result, $offset);
    my $buf = $self->{buf};

    my $numeric   = $1 if $self->{context} =~ m/(\d+)/;
    my $context   = $context{$self->{current_lang}}{$self->{context}} || $numeric;
    my $overflow  = $self->{overflow}->{$self->{context}} || $self->{max_context};

    my $reject = ($self->{current_lang} eq 'g') ?
      qr /^\.\ |^.\.\.|^[ &][A-Za-z]|^[a-z][a-z]\.|^.\$|^..@\d|^...[&\$]|^..._|^..\.[ \]]\d/ :
        qr /^\.\ |^.\.\.|^\ [A-Za-z]|^fr\.|^cf\.|^eg\.|^.\$|^..@\d|^...[&\$]|^..._|^..\.[ \]]\d|^[A-Z]|^.[A-Z]/;

    # The blocks containing the work in
    # question have been read into memory, so all we need to do
    # is to start at the beginning of the correct block
    $offset = $self->{seen}[$match][1];
    $beg = $self->{seen}[$match][0];
    my $buf_start  = (($offset >> 13) << 13);

    # Get context block
    if ($self->{numeric_context}) {
        # Get a number of lines of context before the match -- the concept of
        # `line' here is very unsophisticated.
        for ($lines = 0; $lines <= $context and
             ($beg > 0); $beg--) {
            $lines++ if (((ord (substr ($$buf, $beg, 1))) >> 7) and not
                         (ord (substr ($$buf, $beg - 1, 1)) >> 7));
        }
        $beg+=2;
    }
    else {
        # Find the start of the word pattern -- our pattern might have
        # punctuation inside, or may simply have picked some up along the way.
        for ($beg--; $beg > 0; $beg--) {
            last if substr ($$buf, $beg, 2) =~ /[a-zA-Z)(=\\\/+][\s.,;:]/;
        }
        # Get a regexp-delimited context
        for ($lines = 0; $beg > 0; $beg--) {
            # Lookahead so as to avoid ?_, @1, $., .&, Q. Horatius usw.
            last if substr ($$buf, $beg, 1) =~ /$context/
              and not substr ($$buf, $beg - 2, 5) =~ $reject;
            # Failsafe
            $lines++ if (((ord (substr ($$buf, $beg, 1))) >> 7) and not
                         (ord (substr ($$buf, $beg - 1, 1)) >> 7));
            last if $lines >= $overflow;
        }
        # Try to eliminate stray chars and wasteful whitespace
        #while (substr ($$buf, ++$beg, 1) =~ /[\s\d'"@}\])&\$\x80-\xff]/) {};
        1 while (substr ($$buf, ++$beg, 1) =~ /[\s\d'"@}\])\x80-\xff]/);
    }

    my $buflen = length($$buf);
    if ($self->{numeric_context}) {
        # Get lines of context after the match.
        for ($end = $offset, $lines = 0; ($lines <= $context) and
             ($end < $buflen ) ; $end++) {
            $lines++ if (((ord (substr ($$buf, $end, 1))) >> 7) and not
                         (ord (substr ($$buf, $end + 1, 1)) >> 7));
        }
        $end-=2;
    }
    else {
        # Get a regexp-delimited context
        for ($end = $offset, $lines = 0; $end < $buflen; $end++) {
            # Lookahead so as to avoid ?_, @1, $. usw.
            last if substr ($$buf, $end, 1) =~ /$context/
              and not substr ($$buf, $end - 2, 5) =~ $reject;
            # Failsafe
            $lines++ if (((ord (substr ($$buf, $end, 1))) >> 7) and not
                         (ord (substr ($$buf, $end + 1, 1)) >> 7));
            last if $lines >= $overflow;
        }
        $end++;
    }

    $result = substr($$buf, $beg, ($end - $beg));

    return ($beg, $end, \$result);
}

sub prepare_output
{
    my ($self, $beg, $result_ref) = @_;
    my $buf = $self->{buf};
    my ($location, $this_work, $output);

    if (exists $self->{blacklisted_works}
        and $self->{blacklisted_works}{$self->{file_prefix}}{$self->{auth_num}}{$self->{work_num}})
    {
        warn "Warning: supressing hit in blacklisted work ($self->{auth_num}: $self->{work_num})\n";
        $self->{blacklisted_hits}++;
        return 0;
    }

    # Add spaces to start of line for proper indent
    my $spaces = 0;
    my $pre_start = $beg;
    $spaces++ until ord(substr($$buf, --$pre_start, 1)) >> 7;
    $$result_ref = ' ' x $spaces . $$result_ref;

    $location = "\n\&";
    # extract and print the author, work and location of the match
    #print STDERR ">>".$self->{type}.$self->{auth_num}."||\n";

    $this_work = "$author{$self->{type}}{$self->{auth_num}}, ";
    $this_work .=
      "$work{$self->{type}}{$self->{auth_num}}{$self->{work_num}} ($self->{auth_num}, $self->{work_num})";
    $location .= ($self->{print_bib_info} and not
                  $self->{bib_info_printed}{$self->{auth_num}}{$self->{work_num}})
      ? $self->get_biblio_info($self->{type}, $self->{auth_num}, $self->{work_num})
        : $this_work;
    $self->{bib_info_printed}{$self->{auth_num}}{$self->{work_num}} = 'yes'
      if $self->{print_bib_info};

    $location .="\&\n";

    my $jumpto = $self->{type}.','.$self->{auth_num}.','.$self->{work_num};

    foreach (reverse sort keys %{ $self->{level} })
    {
        if ($level_label{$self->{type}}{$self->{auth_num}}{$self->{work_num}}{$_})
        {
            $location .=
              "$level_label{$self->{type}}{$self->{auth_num}}{$self->{work_num}}{$_}".
                " $self->{level}{$_}, ";
            $jumpto .= ':'.$self->{level}{$_};
        }
        elsif ($self->{level}{$_} ne '1')
        {       # The Theognis exception & ddp
            $location .= "$self->{level}{$_}, ";
            $jumpto .= ':'.$self->{level}{$_};
        }
    }
    chop ($location); chop ($location);
    if ($self->{special_note})
    {
        $location .= "\nNB. $self->{special_note}";
        undef $self->{special_note};
    }

    $location .= "\n\n";
    if ($Diogenes::Base::cgi_flag and $self->{cgi_buttons})
    {
        # Leading space keeps it out of the perseus links
        $$result_ref .= " \n~~~$jumpto~~~\n";
    }
    else
    {
        $$result_ref .= "\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
    }
    my $base = ($self->{current_lang} eq 'g')
      ? '$'
        : '&';
    $output = '& ' . $location . $base . $$result_ref;

    return \$output;
}

sub do_reject
{
    my ($self, $buf) = @_;
    if (not ref $self->{reject_pattern}) {
        return $$buf =~ m/$self->{reject_pattern}/;
    }
    else {
        my $val = $self->matches($buf, $self->{reject_pattern});
        undef pos($$buf) unless $val;
        return $val;
    }
}


{

=begin comment

These subroutines perform variable-length look-behind.

Ensure that the string matched occurs at a word-boundary, i.e. at the beginning
of a word, on a strict match. After the pattern is matched, a substring off the
end of ${^PREMATCH} is passed here and reversed.

=cut

# Skip instances of recursive { ... { ... } ... }, backwards
my $inter_tok_rx = qr< (
                         \}
                         [^}{]*+
                         (?:
                             \{
                            |
                             (?-1)
                         )
                       )
                     >x;

sub strict_lookback {
    my $buf = scalar reverse shift;
    my $match = $buf =~ m<^
                           (?:
                               [^-${IsBetaChr}'}{ _]++
                              |
                               (?<space>[ _])
                              |
                               $inter_tok_rx
                           )*+
                           (?<char> [-${IsBetaChr}'] )
                         >x;

=begin comment

If a space intervenes between the match and the first preceding beta-letter or
-diacritic, return true, unless a dash also intervenes. Return true if the match
occurs at the beginning of the author file.

=cut

    return 1 if ! $match or $+{space} && $+{char} ne "-";
    return 0;
}

=begin comment

On strict matches, if the string to be matched begins with a vowel that does
not necessarily fall at the beginning of a word and is not marked by a
breathing, accent, or other diacritic (including an iota-subscript), ensure
that the vowel doesn't match the same vowel that *is* marked with a breathing,
accent, etc. Without this check, for example, a search for "ιησοῦ" would match
against "Ἰησοῦς".

=cut

sub protect_vow { scalar(reverse shift) !~ /^[${IsBetaDia}]{1,3}+\*/ }


# Ensure that the string matched occurs at the beginning of a word for loose matching.

# sub lookback
# {
#     my $buf = scalar reverse shift;
#     my $match = $buf =~ m/^(?:[^-${IsBetaLet}'}{ _]++|(?<space>[ _])|$inter_tok_rx)*+(?<char>[-${IsBetaLet}'])/;
#     return (not $match) if not $match;
#     return $match if $+{space} and ($+{char} ne "-");
#     return not $match;
# }

sub lat_lookback {
    my $buf = scalar reverse shift;
    my $match = $buf =~ m<^
                           (?:
                               [^ _\-&${IsLatLet}}{]++
                              |
                               (?<space>[ _])
                              |
                               $inter_tok_rx
                           )*+
                           (?<char> [-${IsLatLet}&{] )
                         >x;
    return 1 if ! $match or $+{space} && $+{char} ne "-";
    return 0;
}

}


1;
