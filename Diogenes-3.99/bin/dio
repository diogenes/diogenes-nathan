#!/usr/bin/env perl

=begin comment

Diogenes is a set of programs including this script which provides a
command-line interface to the CD-Rom databases published by the Thesaurus
Linguae Graecae and the Packard Humanities Institute.

Send your feedback, suggestions, and cases of fine wine to
P.J.Heslin@durham.ac.uk

Copyright P.J. Heslin 1999-2000.  All Rights Reserved.

This module is free software.  It may be used, redistributed, and/or modified
under the terms of the GNU General Public License, either version 2 of the
license, or (at your option) any later version.  For a copy of the license,
write to:

  The Free Software Foundation, Inc.
  675 Massachussets Avenue
  Cambridge, MA 02139
  USA

This program is distributed in the hope that they will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.

=end comment

=cut

BEGIN { $usage = <<USAGE; print $usage and exit unless @ARGV }

usage: dio [options] pattern [pattern [pattern ... ]]

Options:

    -g  Search TLG database of Greek texts (default).
    -l  Search PHI database of Latin texts.
    -i  Search the corpus of Classical inscriptions.
    -I  Search the corpus of Christian inscriptions.
    -p  Search the corpus of Duke documentary papyri.
    -M  Search the miscellaneous PHI texts (Vulgate, Milton, etc.).
    -B  Search the TLG bibliographical data.
    -w  TLG word-list seach.

    --strict  With -g, force strict matching for following patterns.

  In combination with one of the above options:

    -n  (num or pattern) Specify author name or number.
    -b  Same as above, but run the text browser.
    -Z  Same as above, but print the specified work in its entirety.

    -o  (filename) Dump output to file.
    -v  Print verbose help and exit.

USAGE

use strict;
use warnings;
use integer;

use Diogenes::Base qw(%encoding %context @contexts %choices %work %author);
use Diogenes::Search;
use Diogenes::Indexed;
use Diogenes::Browser;

our ($opt_l, $opt_w, $opt_g, $opt_b, $opt_r, $opt_p, $opt_O, $opt_R,
     $opt_c, $opt_d, $opt_v, $opt_x, $opt_f, $opt_n, $opt_a, $opt_z,
     $opt_i, $opt_I, $opt_M, $opt_B, $opt_e, $opt_h, $opt_m, $opt_N,
     $opt_C, $opt_u, $opt_U, $opt_W, $opt_o, $opt_D, $opt_P, $opt_F,
     $opt_8, $opt_7, $opt_3, $opt_k, $opt_j, $opt_t, $opt_Z, $opt_y, $opt_Y, $opt_A);

## Make %ENV safer
delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};
$ENV{PATH} = "/bin/:/usr/bin/";

my ($inp, @wlist);
my (%args, @patterns);
my ($a1, $aw, $b1, $bw, $c, $cw, $d) = ('') x 7; # format variables
my ($query, $old_fh);

process_args(\%args, \@patterns);

if ($opt_b || $opt_Z) {
    $query = new Diogenes::Browser(%args);
    die "Database not found!\n" unless ref $query;
    browser();
    exit;
}
elsif ($opt_w) {
    $query = new Diogenes::Indexed(%args);
    die "Database not found!\n" unless ref $query;
}
else {
    $query = new Diogenes::Search(%args);
    die "Database not found!\n" unless ref $query;
}

die "Cannot find database for $args{type}\n" unless $query->check_db;

my %req;
if ($opt_F) {
    my %labels = %{ $query->select_authors(get_tlg_categories => 1) };

    my ($num, @inputs);
    $/ = "\n";
  INP:
    for my $label (keys %labels) {
        next if $label eq 'auths';
        print "\nHere are the choices for $label:\n\n";
        print "Cross-indexed genre listing (somewhat more broadly\ninclusive than the genre listings)\n\n" if $label eq 'genre_clx';
        my $last = $#{ $labels{$label } };
        my $half = int( $last / 2 );
        local $~ = 'COLS';
        ($a1, $b1, $c, $d) = (0, '', '', '');
        my $n = 0;
        while ($n <= $half and $last > 0) {
            $a1 = "$n)";
            $b1 = @{ $labels{$label} }[$n];
            $c = $n + $half.')';
            $d = @{ $labels{$label} }[$n + $half];
            write;
            $n++;
        }
        if ($last % 2 or $last == 0) {
            $a1 = "$last)";
            $b1 = @{ $labels{$label} }[$last];
            ($c, $d) = ('', '');
            write;
        }
        if ($label eq 'date') {
            my ($varia, $incertum, $begin, @dates);
            for (0 .. $#{ $labels{date} }) {
                $varia    = $_ if @{ $labels{date} }[$_] eq 'Varia';
                # Note the space at the end of Incertum
                $incertum = $_ if @{ $labels{date} }[$_] eq 'Incertum ';
            }
            $inp = '';
            print "Beginning of (inclusive) date range: ";
            chomp ($inp = <STDIN>);
            if (($begin = $inp) ne '') {
                print "End of (inclusive) date range: ";
                chomp ($inp = <STDIN>);
                $inp ||= $begin;
                @dates = ($labels{date}[$begin], $labels{date}[$inp]);
                unless ($inp eq '' or $inp == $varia or $inp == $incertum) {
                    print "Include Varia and Incerta? [n] ";
                    chomp ($inp = <STDIN>);
                    push @dates, 1, 1 if $inp =~ /y/i;
                }
            }
            @{ $req{date} } = @dates if $begin ne '';
        }
        else {
            print "Enter the corresponding number(s) for $label (Enter to ignore): ";
            chomp ($inp = <STDIN>);
            if ($inp ne '') {
                # validate input, multiple
                @inputs = ();
                for $num (split /[\s,]+/, $inp) {
                    if (@{ $labels{$label} }[$num]) { push @{ $req{$label} }, @{ $labels{$label} }[$num] }
                    else {
                        print "What?\n";
                        redo INP;
                    }
                }
            }
        }
    }
    print "author regexp to match: ";
    chomp ($inp = <STDIN>);
    $req{author_regex} = $inp if $inp ne '';

    print "author nums (whitespace separated): ";
    chomp ($inp = <STDIN>);
    $req{author_nums} = [split /[\s,]+/, $inp] if $inp ne '';

    print "criteria to match [any]: ";
    chomp ($inp = <STDIN>);
    $req{criteria} = ($inp eq '') ? 1: ($inp =~ m/any/i) ? 'any': ($inp =~ m/all/i) ? 'all' : $inp;

    if ((not %req) or (exists $req{criteria} and $req{criteria} eq '0')) {
        print "No criteria given; searching entire corpus. \n"
    }
    else {
        # print Data::Dumper->Dump ([\%req], ['*req']);
        print "\nHere are the matching authors and works:\n\n";
        my @auths = $query->select_authors(%req);
        # print Data::Dumper->Dump ([\@auths], ['*auths']) if %req;
        local $~ = 'AUTHS';
        #print "$_)\t$auths[$_] \n" for (0 .. $#auths);
        for (0 .. $#auths) {
            $a1 = "$_)";
            $b1 = "$auths[$_]";
            write;
        }
        print "Which of these texts should be searched? [Return for all]: ";
        chomp ($inp = <STDIN>);
        if ($inp ne '') {
            undef %req;
            $req{previous_list} = [split /[\s,]+/, $inp];
            @auths = $query->select_authors(%req);
            print "\nSearching within the following texts: \n\n";
            print "$auths[$_] \n" for (0 .. $#auths);
            print "\n";
            # print Data::Dumper->Dump ([\@auths], ['*auths']);
        }
    }
}
elsif ($opt_f) {
    my (%auths, @auths);
    if ($opt_n) { @auths = $query->select_authors(author_regex => $opt_n) }
    else {
        %auths = %{ $query->select_authors('select_all' => 1) };
        local $~ = 'AUTHS';
        print "\n";
        for (sort keys %auths) {
            $a1 = "$_)";
            $b1 = "$auths{$_}";
            write;
        }
        print "Which of these texts should be searched? [Return for all]: ";
        chomp ($inp = <STDIN>);
        if ($inp ne '') {
            undef %req;
            $req{author_nums} = [split /[\s,]+/, $inp];
            @auths = $query->select_authors(%req);
        }
    }
    print "\nSearching within the following texts: \n\n";
    print "$auths[$_] \n" for (0 .. $#auths);
    print "\n";
}
elsif ($opt_n) {
    if ($opt_n =~ m/\d/) {
        my @nums = split /[\s,]+/, $opt_n;
        $req{author_nums} = \@nums;
    }
    else { $req{author_regex} = $opt_n }

    my @auths = $query->select_authors(%req);
    print "\nSearching within the following texts: \n\n";
    print "$auths[$_] \n" for (0 .. $#auths);
    print "\n";
}
elsif ($opt_N) {
    # We use a temp browser to find the works
    my $browser = new Diogenes::Browser(%args);
    die "Database not found!\n" unless ref $query;
    my %auths;
    if ($opt_N =~ m/\d/) {
        %auths = (%auths, $browser->browse_authors($_)) for split /[\s,]+/, $opt_N;
    }
    else { %auths = $browser->browse_authors($opt_N) }

    #print Data::Dumper->Dump ([\%auths], ['auths']);
    if (%auths) {
        print "\nFor each author, enter the number(s) of the work(s) to be searched: \n";
    }

    my %works;
    for my $auth (sort keys %auths) {
        print "\n$auths{$auth}:\n";
        my %all_works = $browser->browse_works ($auth);
        print "  $_: $all_works{$_}\n" for (sort keys %all_works);
        print "\n  Work? (Return for all, 0 for none) ";
        $/ = "\n";
        chomp ($inp = <STDIN>);
        unless ($inp eq "0") {
            $works{$auth} = ($inp eq '') ? 'all' : [split /[\s,]+/, $inp];
        }
    }
    print "\n";
    #print Data::Dumper->Dump ([\%works], ['works']);
    my @auths = $query->select_authors(author_nums=>\%works);
    print "\nSearching within the following texts: \n\n";
    print "$auths[$_] \n" for (0 .. $#auths);
    print "\n";
}

if ($opt_w) {
    my @word_lists;
    for my $pattern (@patterns) {
        my ($wref, @wlist) = $query->read_index($pattern);
        die "Sorry, I didn't find anything in the word list!\n" unless @wlist;
        unless ($opt_a) {
            print "\nHere are the words from the word list that match \n".
              "the word-beginning you gave:\n\n";
            my $n;
            if (@wlist > 10000) { # Max in 4 col. num. column
                print "\n";
                print "$_] $wlist[$_] ($wref->{$wlist[$_]})\n" for (0 .. $#wlist)

            }
            else {
                local $~ = 'WORDS';
                for ( $n = 0; $n < @wlist; $n++ ) {
                    $aw = $wlist[$n] . " ($wref->{$wlist[$n]})";
                    $a1  = "$n]";
                    last if ++$n >= @wlist;
                    $bw = $wlist[$n] . " ($wref->{$wlist[$n]})";
                    $b1  = $n . ']';
                    last if ++$n >= @wlist;
                    $cw = $wlist[$n] . " ($wref->{$wlist[$n]})";
                    $c =  $n . ']';
                    write;
                    ($a1, $aw, $b1, $bw, $c, $cw) = ('', '', '', '', '', '');
                }
                write;
            }
            print "\nEnter a list of desired words (or Return for all): ";
            chomp ($inp = <STDIN>);
            print "\n";
            @wlist = @wlist[split /[\s,]+/, $inp] unless ($inp eq '');
        }
        push @word_lists, \@wlist;
    }
    print "Sending output to $opt_o.\n" if $old_fh;
    if ($old_fh) { select OUT_FILE; $| = 1; }
    $query->do_search(@word_lists);
}
else {
    print "Sending output to $opt_o.\n" if $old_fh;
    if ($old_fh) { select OUT_FILE; $| = 1; }
    $query->do_search;
}

print "done.\n\n";

sub browser {
    my (%auths, $auth, %works, $work, @labels, $j, $lev,  @target);
    $query->{browse_lines} = $opt_c if $opt_c;
    $| = 1 if $opt_h; # Flush buffer if we are viewing this output
    my $fh = select STDERR if ($opt_x) ;

    # prompt for choice of author
    print "\n";
    my $browse_pattern = (shift @patterns) || '';
    $browse_pattern = $opt_n if $opt_n;
    %auths = $query->browse_authors($browse_pattern);
    die "Sorry. No matching authors. \n" unless %auths;
    print "Please select one:\n\n" if keys %auths > 1;
    print "$_: $auths{$_}\n" for sort keys %auths;

    print "\n";
    if (keys %auths == 1) { $auth = (keys %auths)[0] }
    else {
        print $query->{type} eq 'bib' ? 'Bibliography? ' : 'Author? ';
        $/ = "\n";
        $auth = <>;
        chomp $auth;
        $auth = sprintf '%04d', $auth =~ /(\d+)/ unless $auth =~ /\D/ or $query->{type} eq 'bib';
    }

    # prompt for choice of work
    %works = $query->browse_works ($auth);
    die "Sorry. No matching works. \n" unless %works;
    print "Please select one:\n\n" if keys %works > 1;

    print "$_: $works{$_}\n" for sort keys %works;

    print "\n";
    if (keys %works == 1) { $work = (keys %works)[0] }
    else {
        print "Work? ";
        $/ = "\n";
        $work = <>;
        chomp $work;
        $work = sprintf '%03d', $work =~ /(\d+)/ unless $work =~ /\D/;
        for (sort keys %works) {
            if ($work lt $_) {
                $query->{next_work_num} = $_;
                last;
            }
        }
    }
    print "\n";

    if ($opt_Z) { @target = (0) x 100 }
    else {
        # prompt for location in the text
        @labels = $query->browse_location ($auth, $work);
        $j = (scalar @labels) - 1;
        print "Enter the number of any TLG author: \n" if $query->{type} eq 'bib';
        for $lev (@labels) {
            next if $lev =~ m#^\*#;
            print "$lev? ";
            $/ = "\n";
            $inp = <>;
            chomp $inp;
            push @target, $inp;
            $j--;
        }
        print "\n";
    }
    # show the desired text
    print "Sending output to $opt_o.\n" if $old_fh;
    if ($old_fh) { select OUT_FILE; $| = 1; }
    $query->seek_passage ($auth, $work, @target);
    $query->begin_boilerplate;
    if (grep {m/^0$/ or m/^$/} @target) { $query->browse_forward }
    else { $query->browse_half_backward }
    print "<p>\n" if $opt_h;
    # select STDERR if $old_fh;

    unless($opt_Z) {
        # prompt reader to move back or forth in the text
        $inp = "";

        until ($inp =~ /[qQ]/) {
            select STDERR if $old_fh;
            print "\n[more], [b]ack, [q]uit? ";
            local $/ = "\n";
            $inp = <>;
            print "\n";
            if ($inp =~ /[Bb]/) {
                print "Sending output to $opt_o.\n" if $old_fh;
                if ($old_fh) {
                    select OUT_FILE; seek OUT_FILE, 0, 0; truncate OUT_FILE, 0; $| = 1;
                }
                $query->browse_backward;
            }
            else {
                print "Sending output to $opt_o.\n" if $old_fh and $inp !~ /[qQ]/;
                if ($old_fh) { select OUT_FILE; $| = 1; }
                $query->browse_forward unless ($inp =~ /[qQ]/);
            }
            print "\n";
            print "<p>\n" if $opt_h;
        }
    }
    print "\n";
    $query->end_boilerplate;
}

sub process_args {

    use Getopt::Std;
    use Encode;

    my ($args, $patterns) = @_;

    getopts('FWwlgbriIpyYRdvMCxfaAzBehu873tZPN:n:c:m:U:o:O:D:k:j:');

    display_help() if $opt_v;
    unless (@ARGV) { print $::usage and exit(1) }

    # Die with -o when file already exists. overwrite file with -O.
    if (defined($opt_o) && -f $opt_o) {
        die "File $opt_o already exists. Override with -O instead of -o.\n";
    }
    elsif (defined($opt_o //= $opt_O)) {
        open(OUT_FILE, '>', $opt_o) || die "could not open $opt_o: $!";
        $old_fh = select STDERR;
    }

    $args->{type} = 'tlg'  if $opt_g;
    $args->{type} = 'phi'  if $opt_l;
    $args->{type} = 'ddp'  if $opt_p;
    $args->{type} = 'ins'  if $opt_i;
    $args->{type} = 'chr'  if $opt_I;
    $args->{type} = 'misc' if $opt_M;
    $args->{type} = 'bib'  if $opt_B;
    $args->{type} = 'cop'  if $opt_C;

    $args->{input_encoding} = 'Perseus-style'   if $opt_P;
    $args->{input_encoding} = 'BETA code'       if $opt_t or $opt_r or $opt_z;
    $args->{input_encoding} ||= 'Unicode';
    $args->{input_raw}      = $opt_r;
    $args->{input_pure}     = $opt_z;

    if ($args->{input_encoding} eq 'Unicode') {
        @$patterns = map { decode(UTF_8 => $_, 1) } @ARGV;
    }
    else { @patterns = @ARGV }

    $args->{output_format} = 'repaging', $args->{encoding} = 'Ibycus' if $opt_R;
    $args->{output_format} = 'latex', $args->{encoding} = 'Ibycus', $args->{printer} = 1 if $opt_x;
    $args->{output_format} = 'ascii', $args->{encoding} = 'Transliteration' if $opt_A;
    $args->{output_format} = 'beta' if $opt_Y;
    $args->{output_format} = 'html' if $opt_h;

    $args->{debug}      = $opt_d;
    $args->{bib_info}   = $opt_e;
    $args->{context}    = $opt_c if ($opt_c && !$opt_b);
    $args->{min_matches} = $opt_m if $opt_m;
    $args->{min_matches} ||= 'all';

    $args->{encoding} = $opt_U              if $opt_U;
    $args->{encoding} = 'Unicode_Entities'  if $opt_u;
    $args->{encoding} = 'UTF-8'             if $opt_8;
    $args->{encoding} = 'ISO_8859_7'        if $opt_7;
    $args->{encoding} = 'Beta'              if $opt_Y or $opt_y;
    $args->{encoding} = 'DOS_CP_737'        if $opt_3;
    $args->{encoding} = 'WinGreek'          if $opt_W;

    $args->{tlg_dir} = $args->{phi_dir} = $args->{ddp_dir} = $opt_D if $opt_D;
    $args->{encoding}       = 'UTF-8'   if $opt_h and not $args->{encoding};
    $args->{blacklist_file} = $opt_k    if $opt_k;
    $args->{quiet}          = 1         if $opt_Z;
    $args->{reject_pattern} = decode(UTF_8 => $opt_j, 1) if $opt_j;
    if (!$opt_b && $args->{input_encoding} ne 'Unicode' && ($opt_i || $opt_I || $opt_M)) {
        # if the language cannot be determined by the input:
        if ($opt_g || $opt_t) { $args->{input_lang} = 'g' }
        elsif ($opt_l) { $args->{input_lang} = 'l' }
        else { die "Cannot determine input language. Specify -g or -l.\n" }
    }

    $args->{pattern_list} = $patterns unless $opt_b;
    undef @ARGV;

    die "Edition info only available for the TLG. \n" if $opt_e and not ($opt_g or $opt_w);

    return;
}

sub display_help {
    if ($^O =~ /MSWin|dos/) {
        use Pod::Text ();

        Pod::Text->new->parse_file(__FILE__);
    }
    else {
        use Pod::Man ();
        my $f;

        open($f, '|-', 'man',  '-l', '-') || die $!;
        Pod::Man->new->parse_from_file(__FILE__, $f);
    }

    exit;
}

format COLS =
@<<<<<@<<<<<<<<<<<<<<<<<<<<<<<<  @<<<<<@<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$a1, $b1, $c, $d
.
format AUTHS =
@<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$a1,     $b1
~        ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        $b1
.
format WORDS =
@<<< @<<<<<<<<<<<<<<<<<< @<<< @<<<<<<<<<<<<<<<<< @<<< @<<<<<<<<<<<<<<<<<<
$a1, $aw,           $b1, $bw,          $c, $cw
.

# ex: set shiftwidth=4 tw=78 nowrap ts=4 si sta expandtab: #:w

__END__

=head1 NAME

dio - command-line interface to the I<Diogenes> suite

=head1 SYNOPSIS

S<dio [options] pattern [pattern [pattern ... ]]>

=head1 DESCRIPTION

This is I<Diogenes>, a search tool for the databases of Latin and Greek
published on CD-Rom by the I<Packard Humanities Institute> and the I<Thesaurus
Linguae Graecae>.

=head1 CORPUS OPTIONS

=over

=item B<-g>

Do a brute force search of the TLG (the scope may be narrowed down with B<-f> or
B<-n>, for which, see below).  This is the default behavior.  When a given
search pattern contains one or more diacritic marks (including the
iota-subscript), matching for that pattern will be performed strictly, except
that acute and grave accents are interchangeable; otherwise, diacritics in the
text to be matched will be ignored, unless B<--strict> precedes.

=item B<--strict>

With B<-g>, force all of the following patterns to be interpreted strictly,
i.e., with accents and breathing marks significant.  This is necessary for
forcing strict interpretation for patterns that do not contain diacritic marks,
all of which would otherwise be interpreted loosely.

=item B<-w>

Run a TLG word-list search.  The search pattern may contain breathing marks and
accents, but they will not be considered.  A list of full and partial matches
will be displayed, and the user will be prompted to select one or several.
Search the entire TLG by default; limit with B<-n>, for which, see below.

=item B<-a>

With B<-w>, search for all words in the word-list that match the pattern,
without prompting for the user to make a selection.

=item B<-e>

Add some info about the edition of each text to the citations: (TLG only, but
there is some bibliographical info on the papyri, inscriptions and Latin canon
that can be accessed using the browser).

=item B<-l>

Search the PHI Latin corpus.  I and J, and U and V, are interchangeable.

=item B<-i>

Search the corpus of Classical inscriptions.

=item B<-I>

Search the corpus of Christian inscriptions.

=item B<-p>

Search the Duke documentary papyri.

=item B<-M>

Search the miscellaneous texts on later versions of the PHI disk (Hebrew bible,
Vulgate, Milton, etc.).

=item B<-C>

Search the Coptic texts on the PHI disk.

=item B<-B>

Search the TLG bibliographical data.

=item B<-b>

Run the text browser. Supply a pattern to narrow the options down to a
particular text.

=item B<-f>

Interactively filter the texts in which to search.

=item B<-F>

Like B<-f>, but filter by genre, range of dates, etc.; the TLG alone provides
this type of information.

=item B<-n> I<number> or I<pattern>

Filter texts, non-interactively, by author names or numbers.  For example, S<dio
-gn Homer ...> would search only in those authors whose names match the pattern
`Homer', which includes the Hymns, Homeric scholia and a few other related
texts.  To be more precise, use a number instead of letters with this option,
and it will be interpreted as an author number or list of author numbers.  The
number of Homer himself is 0012, while the Homeric Hymns are 0013, and so S<dio
-gn 12,13 ...> will search only those two files.

=item B<-k> I<filename>

A file containing a blacklist of authors not to be searched.  Syntax of the file
is: S<"tlg2062 lat0474"> to block out John Chrysostom and Cicero.

=item B<-j> I<pattern>

A pattern which, if it is found in the context of a hit, will cause that hit not
to be reported.

=back

=head1 INPUT ENCODING OPTIONS

For all patterns entered on the command line, case is insignificant, and
white-space is regularized.  Except for -w, white-space at the beginning and end
of a given pattern is interpreted to indicate that the pattern can only match at
the beginning and end of a word, respectively.

As of version 3.1, default input encoding is UTF-8 Unicode, and Diogenes will
infer therefrom whether to generate a Greek or Latin regular expression.  When
entering diacritics, use only composite forms.

=over

=item B<-P>

Interpret patterns according to the Latin transliteration of the Greek and Latin
alphabets as used by the Perseus project.  Use in conjunction with B<-g> or
B<-l>--even when not searching through one of those corpora--to indicate the
input language.  See http://nlp.perseus.tufts.edu for more information.

=item B<-t>

Interpret patterns as BETA code (Greek only).  See http://tlg.uci.edu/encoding
for more information.

=back

=head1 OUTPUT OPTIONS

=over

=item B<-8>

Output UTF-8 Unicode text.  This is the default.

=item B<-A>

Output 7-bit ascii text only, similar to Ibycus, but somewhat cleaner.  See also
B<-y>.

=item B<-U> I<encoding>

Use the named encoding for Greek output, which should be defined in a
Diogenes.map file somewhere in the path Perl searches.

=item B<-W>

Output WinGreek-encoded text for Greek output.

=item B<-7>

Output ISO-8859-7 Modern (monotonic) Greek for Greek output.

=item B<-3>

Output MS-DOS/Windows Modern (monotonic) Greek code page 737 for Greek output.

=item B<-y>

Output text as beta code.

=item B<-Y>

Output text as raw beta code with binary formatting data intact.

=item B<-x>

Output a (messy and unreadable) LaTeX file.  This attempts to preserve and
correctly represent many of the font, special character and layout formatting
codes recorded with the texts.  Example usage:

S<< dio -wax foo >myfile.tex; latex myfile; xdvi myfile >>

The file produced by B<-x> will use Ibycus as its Greek encoding, not Babel.
For Babel output, use -UBabel_7 and paste the output into a LaTex file that
declares: \usepackage[iso-8859-7]{inputenc} \\usepackage[polutonikogreek]{babel}
The result should represent the text accurately, but will not attempt to
preserve all of the formatting detail that B<-x> gives you.

=item B<-o> I<filename>

A file to dump the output to.  Interactive queries go to STDERR instead of this
file.

=item B<-c> I<val>

Show val amount of context before and after each match.  If val is a number, it
is the number of lines to display before and after each match; this is also the
maximum distance apart of patterns that are guaranteed to be matched in multiple
matching mode (it may be that some patterns even farther apart may also be
caught).  Otherwise, val should be either `sent', `clause', `phrase' or `para'.
The default is sentence mode, where an attempt is made to determine the
appropriate scope from the punctuation.  With B<-b>, val specifies instead the
number of lines (approximately) to be printed by the browser.

=item B<-Z>

Output a work in its entirety.  With B<-b>, use B<-c> (above) to control
periodicity of browser output.

=item B<-D> I<drive> or I<mount point>

Use CD-Rom found here, overriding the values found in the configuration files.

=item B<-h>

Format the output using HTML.

=item B<-u>

With B<-h>, use the Unicode HTML entities encoding for Greek output.

=back

=head1 DEBUGGING OPTIONS

=over

=item B<-d>

Print copious debugging information during program execution.

=item B<-z>

Interpret input patterns as raw Perl regular expressions to be compiled and
executed against the specified corpus.

=item B<-r>

Interpret the input as a raw string to be matched exactly.  (I.e., escape all
meta-characters.)

=back

=head1 ENVIRONMENT

The following environment variables effect I<dio>'s behaviour.

=over

=item B<PERSEUS_DATA>

Directory containing the Perseus data.

=item B<Diogenes_Perseus_Dir>

Deprecated. Superceded by PERSEUS_DATA.

=back

=head1 AUTHORS

Peter Heslin C<< <p.j.heslin@durham.ac.uk> >>

Nathan Trapuzzano C<< <nbtrap@nbtrap.com> >>

=head1 LICENSE

This module is free software.  It may be used, redistributed, and/or modified
under the terms of the GNU General Public License, version 2 or later.

=head1 COPYRIGHT

Copyright (C) P.J. Heslin 1999-2000.  All Rights Reserved.

=cut
